﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MVCwWebApi.Startup))] // el que usa mvc

//[assembly: OwinStartup(typeof(MVCwWebApi.Startup))] // el que usa webapi
namespace MVCwWebApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
