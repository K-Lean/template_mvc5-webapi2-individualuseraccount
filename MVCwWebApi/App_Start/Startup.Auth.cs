﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using MVCwWebApi.Providers;
using Owin;
using MVCwWebApi.Models;




namespace MVCwWebApi
{
    public partial class Startup
    {

        /* WebApi Authorization - Configuracion OAuthOptions + Token EndPoint */
        static Startup()
        {

            PublicClientId = "self";

            //UserManagerFactory = () => new UserManager<IdentityUser>(new UserStore<IdentityUser>());
            
            // atributos custom para el Api User. Le pasamos el mismo DB context de MVC para que lo use la API
            UserManagerFactory = () => new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));

            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                Provider = new ApplicationOAuthProvider(PublicClientId, UserManagerFactory),
                AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
                AllowInsecureHttp = true
            };



        }

        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        //public static Func<UserManager<IdentityUser>> UserManagerFactory { get; set; }

        // Lean: Custom User
        public static Func<UserManager<ApplicationUser>> UserManagerFactory { get; set; }

        public static string PublicClientId { get; private set; }

        


        /*---------------------*/

        /* Configuración Authentication  */
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            /* Para Web MVC usamos Authentication por Cookies(por defecto activada) y External Coookies*/

            // Enable the application to use a cookie to store information for the signed in user
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login")
            });

            // Use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            /* Para Web Api usamos OAuthBearerTokens Authentication */

            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "http://www.iis.net/learn/manage/configuring-security/how-to-set-up-ssl-on-iis");

            //app.UseTwitterAuthentication(
            //   consumerKey: "",
            //   consumerSecret: "");

            //app.UseFacebookAuthentication(
            //   appId: "",
            //   appSecret: "");

            //app.UseGoogleAuthentication();
        }
    }
}